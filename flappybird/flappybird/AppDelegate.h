//
//  AppDelegate.h
//  flappybird
//
//  Created by Mariuxi Ana Yagual on 4/9/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

