//
//  GameViewController.h
//  flappybird
//
//  Created by Mariuxi Ana Yagual on 4/9/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameViewController : UIViewController

@end
