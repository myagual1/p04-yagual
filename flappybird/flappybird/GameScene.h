//
//  GameScene.h
//  flappybird
//
//  Created by Mariuxi Ana Yagual on 4/9/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameScene : SKScene

@property (nonatomic) NSMutableArray<GKEntity *> *entities;
@property (nonatomic) NSMutableDictionary<NSString*, GKGraph *> *graphs;

@end
